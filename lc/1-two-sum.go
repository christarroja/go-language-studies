package main

import "fmt"

func twoSum(nums []int, target int) []int {
	length := len(nums)
	for i := 0; i < length; i++ {
		num := target - nums[i]
		for a, b := range nums {
			if i != a {
				if num == b {
					return []int{i, a}
				}
			}
		}
	}
	return nil
}

func main() {
	smplNum := []int{2, 7, 11, 15}
	smplTarg := 9
	fmt.Println(twoSum(smplNum, smplTarg))
}
