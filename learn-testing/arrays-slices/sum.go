package main

import "fmt"

func Sum(numbers []int) int {
	sum := 0
	for _, number := range numbers {
		sum += number
	}
	return sum
}

func SumAllTails(numbersToSum ...[]int) []int {
	var sums []int
	for _, numbers := range numbersToSum {
		if len(numbers) == 0 {
			sums = append(sums, 0)
		} else {
			tail := numbers[1:]
			sums = append(sums, Sum(tail))
		}
	}

	return sums
}

func main() {
	test1 := []int{1, 2, 3, 4}
	test2 := []int{5, 6, 7, 8}
	test3 := []int{8, 9, 10}
	fmt.Println(Sum(test1))
	fmt.Println(SumAllTails(test1, test2, test3))
}
