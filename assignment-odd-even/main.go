package main

import "fmt"

func main() {

	sliceOfInts := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, slice := range sliceOfInts {
		if slice%2 == 0 {
			fmt.Printf("%v is even \n", slice)
		} else {
			fmt.Printf("%v is odd \n", slice)
		}
	}
}
