package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		log.Println("Hello Old World")
		d, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(rw, "Opps", http.StatusBadRequest)
			return
		}

		//prints to the server side
		//log.Printf("Data %s\n", d)

		//prints to the user who requested via curl
		fmt.Fprintf(rw, "Hello %s", d)
	})

	http.HandleFunc("/goodbye", func(http.ResponseWriter, *http.Request) {
		log.Println("Goodbye World")
	})

	http.ListenAndServe(":9090", nil)
}
