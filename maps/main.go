package main

import (
	"fmt"

	"github.com/Masterminds/goutils"
)

func main() {
	//var colors map[string]string
	//colors := make(map[string]string)
	item := ""

	fmt.Println(goutils.IsBlank(item))

	colors := map[string]string{
		"red":   "#FF0000",
		"green": "#00FF00",
		"white": "FFFFFF",
	}

	printMap(colors)
}

func printMap(c map[string]string) {
	for color, hex := range c {
		fmt.Println("Hex code for", color, "is", hex)
	}
}
