package main

import "fmt"

type triangle struct {
	height float64
	base   float64
}

type square struct {
	sideLength float64
}

type shape interface {
	getArea() float64
}

func main() {
	t := triangle{3, 4}
	s := square{4}

	printArea(t)
	printArea(s)
}

func printArea(s shape) {
	fmt.Println("The area is:", s.getArea())
}

func (t triangle) getArea() float64 {
	ta := 0.5 * t.base * t.height
	return ta
}

func (s square) getArea() float64 {
	sa := s.sideLength * s.sideLength
	return sa
}
